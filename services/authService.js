const UserService = require('./userService');

class AuthService {
    login(userData) {
      const user = UserService.search(userData);
  
      if (!user) {
        return res.status(404).json({ error: true, message: 'User not found' });
      }
  
      return user;
    }
  }

module.exports = new AuthService();