const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getFighters(res) {
      const fighters = FighterRepository.getAll();
  
      if (!fighters) {
        return res.status(404).json({ error: true, message: 'Fighters not found' });
      }
  
      return fighters;
    }
  
    getFighter(id, res) {
      const fighter = FighterRepository.getOne(id);
  
      if (!fighter) {
        return res.status(404).json({ error: true, message: 'Fighter not found' });
      }
      return fighter;
    }
  
    createFighter(fighterData, res) {
      const searchFields = { name: fighterData.name.toLowerCase() };
      const isBusy = FighterRepository.findByFields(searchFields);
  
      if (!isBusy) {
        const updatedData = { ...fighterData, name: fighterData.name, health: fighterData.health || 100 };
        const fighter = FighterRepository.create(updatedData);
  
        if (!fighter) {
          return res.status(400).json({ error: true, message: 'Can\'t create new fighter' });
        }
  
        return fighter;
      }
  
      return res.status(400).json({ error: true, message: 'A fighter with such data already exists' });
    }
  
    updateFighter(id, fighter, res) {
      const currentFighter = this.getFighter(id);
  
      if (currentFighter) {
        const searchFields = { name: fighter.name };
        const isBusy = FighterRepository.findByFields(searchFields);
  
        if (!isBusy) {
          const updatedFighter = FighterRepository.update(id, fighter);
  
          if (!updatedFighter) {
            return res.status(400).json({ error: true, message: 'Can\'t update fighter' });
          }
  
          return updatedFighter;
        }

        return res.status(400).json({ error: true, message: 'An internal error has occurred' });
      }
  
      return res.status(400).json({ error: true, message: 'A fighter with such data already exists' });
    }
  
    deleteFighterById(id, res) {
      const fighter = this.getFighter(id);
  
      if (fighter) {
        const isDeleted = FighterRepository.delete(id);
  
        if (!isDeleted) {
          return res.status(400).json({ error: true, message: 'An error occurred while removing' });
        }
  
        return isDeleted;
      }
  
      return res.status(400).json({ error: true, message: 'An internal error has occurred' });
    }
  }

module.exports = new FighterService();