const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getUsers(res) {
      const users = UserRepository.getAll();
  
      if (!users) {
        return res.status(404).json({ error: true, message: 'Users not found' });
      }
  
      return users;
    }
  
    getUser(id, res) {
      const user = UserRepository.getOne(id);
  
      if (!user) {
        return res.status(404).json({ error: true, message: 'User not found' });
      }
  
      return user;
    }
  
    createUser(data, res) {
      const searchFields = { email: data.email, phoneNumber: data.phoneNumber };
      const isBusy = UserRepository.findByFields(searchFields);
  
      if (!isBusy) {
        const updatedData = { ...data };
        Object.keys(data).forEach((el) => {
          const element = data[el];
  
          updatedData[el] = element;
        });
  
        const user = UserRepository.create(updatedData);
  
        if (!user) {
          return res.status(400).json({ error: true, message: 'Can\'t create new user' });
        }
  
        return user;
      }
  
      return res.status(400).json({ error: true, message: 'A user with such data already exists' });
    }
  
    updateUser(id, data, res) {
      const user = this.getUser(id);

      if (user) {
        const searchFields = { email: data.email, phoneNumber: data.phoneNumber };
        const isBusy = UserRepository.findByFields(searchFields);
  
        if (!isBusy) {
          const updatedUser = UserRepository.update(id, data);
  
          if (!updatedUser) {
            return res.status(400).json({ error: true, message: 'Can\'t update user' });
          }
  
          return updatedUser;
        }
  
        return res.status(400).json({ error: true, message: 'Fields already have been declared' });
      }
  
      return res.status(400).json({ error: true, message: 'A user with such data already exists' });
    }
  
    deleteUser(id, res) {
      const user = this.getUser(id);
  
      if (user) {
        const isDeleted = UserRepository.delete(id);
  
        if (!isDeleted) {
          return res.status(400).json({ error: true, message: 'An error occurred while removing' });
        }
  
        return isDeleted;
      }
  
      return res.status(400).json({ error: true, message: 'An internal error has occurred' });
    }
  
    search(data) {
      const { email, password } = data;
  
      if (UserRepository.findByFields({ email }) && UserRepository.findByFields({ password })) {
        return true;
      }
  
      return false;
    }
  }

module.exports = new UserService();